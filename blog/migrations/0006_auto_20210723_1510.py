# Generated by Django 3.2.5 on 2021-07-23 13:10

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20210723_1507'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='Username',
            new_name='username',
        ),
        migrations.AlterField(
            model_name='article',
            name='create_date',
            field=models.DateField(default=datetime.datetime(2021, 7, 23, 13, 10, 12, 577124, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='create_date',
            field=models.DateField(default=datetime.datetime(2021, 7, 23, 13, 10, 12, 577124, tzinfo=utc)),
        ),
    ]
