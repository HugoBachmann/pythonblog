from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Article(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField(max_length=5000)
    create_date = models.DateField(default=timezone.now())
    author = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    cover = models.ImageField(upload_to='images/', null=True)


class Comment(models.Model):
    title = models.CharField(max_length=100, default="title")
    content = models.TextField(max_length=1000)
    create_date = models.DateField(default=timezone.now())
    article = models.ForeignKey(Article, null=True, on_delete=models.CASCADE)
    author = models.ForeignKey(User, null=True, on_delete=models.CASCADE)

