from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('archive', views.index, name='archive'),
    path('article/<int:id>/', views.article_single, name='article_single'),
    path('article/new/', views.article_create, name='article_create'),
    path('register', views.register_request, name='register'),
    path('profile/<str:username>/', views.user_profile, name='profile'),
]