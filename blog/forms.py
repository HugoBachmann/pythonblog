from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class User_register(UserCreationForm):
	email = forms.EmailField(required=True)


	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")


	def save(self, commit=True):
		user = super(User_register, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
		return user


class User_login(forms.Form):
    username = forms.CharField(label='username', max_length=100)
    password = forms.CharField(label='password', max_length=128)


class Comment_form(forms.Form):
    title = forms.CharField(label='title', max_length=100)
    content = forms.CharField(label='content', max_length=1000)


class Article_form(forms.Form):
	title = forms.CharField(label='title', max_length=100)
	content = forms.CharField(label='content', max_length=1000)
	cover = forms.FileField()




