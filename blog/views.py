from django.shortcuts import render, redirect
from .models import Article, Comment
from .forms import Comment_form, Article_form, User_register, User_login
from django.contrib.auth import authenticate,  login, logout, get_user_model
from django.contrib import messages
import requests
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone


def index(request):
    latest_article_list = Article.objects.order_by('-create_date')[:10]
    context = {'latest_article_list': latest_article_list}
    return render(request, 'index.html', context)


def archive(request):
    article_list = Article.objects.all()
    context = {'latest_article_list': article_list}
    return render(request, 'index.html', context)


User = get_user_model()


def register_view(request):
    form = User_register(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        password2 = form.cleaned_data.get("password2")
        try:
            user = User.objects.create_user(username, password)
        except:
            user = None
        if user != None:
            login(request, user)
            return redirect("/")
        else:
            request.session['register_error'] = 1
    return render(request, "forms.html", {"form": form})


def login_view(request):
    form = User_login(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            request.session['invalid_user'] = 1
    return render(request, "forms.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("/login")


def article_create(request):
    current_user = request.user
    context = {}
    form = Article_form(request.POST, request.FILES)
    if form.is_valid():
        new_article = Article(title=form.cleaned_data['title'], content=form.cleaned_data['content'], cover=form.cleaned_data['cover'], author=current_user)
        new_article.save()
    context['form'] = form
    return render(request, 'article_create.html', context)


def article_single(request, id):
    user_author = User.objects.get(article=id)
    comments = Comment.objects.filter(article=id)
    article_single = Article.objects.get(pk=id)
    current_user = request.user
    context = {}
    context = {'article': article_single, 'comments': comments, 'author': user_author}
    form = Comment_form(request.POST)
    if form.is_valid():
        new_comment = Comment(title=form.cleaned_data['title'], content=form.cleaned_data['content'], article=article_single, author=current_user)
        new_comment.save()
    context['form'] = form
    return render(request, 'article_single.html', context)


def register_request(request):
	if request.method == "POST":
		form = User_register(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect("/")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = User_register()
	return render (request, 'register.html')


def user_profile(request, username):
    current_user = request.user
    id = current_user.id
    article = Article.objects.filter(author=id)
    user = User.objects.get(username=username)
    context = {'user': user, 'article':article}
    return render(request, 'profile.html', context)
